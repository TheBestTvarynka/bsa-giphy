package com.bsa.bsagiphy.user;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserCacheTest {

    @Test
    void whenGetExistingGifs_thenReturnListWithGifs() {
        String user = "pasha";
        String query = "star";
        List<String> paths = new ArrayList<>();
        paths.add("path/to/star1");
        paths.add("path/to/star2");
        paths.add("path/to/star3");

        UserCache userCache = new UserCache();
        userCache.put(user, query, paths.get(0));
        userCache.put(user, query, paths.get(1));
        userCache.put(user, query, paths.get(2));

        Optional<List<String>> gifs = userCache.getUserGifs(user, query);

        assertTrue(gifs.isPresent());

        List<String> gifsList = gifs.get();
        assertEquals(gifsList, paths);
    }

    @Test
    void whenGetGifsOfUnexpectedUser_thenReturnEmptyOptional() {
        UserCache userCache = new UserCache();

        Optional<List<String>> gifs = userCache.getUserGifs("user", "query");

        assertFalse(gifs.isPresent());
    }

    @Test
    void whenGetGifsOfUnexpectedQuery_thenReturnEmptyOptional() {
        String user = "pasha";
        String query = "star";
        List<String> paths = new ArrayList<>();
        paths.add("path/to/star1");
        paths.add("path/to/star2");
        paths.add("path/to/star3");

        UserCache userCache = new UserCache();
        userCache.put(user, query, paths.get(0));
        userCache.put(user, query, paths.get(1));
        userCache.put(user, query, paths.get(2));

        Optional<List<String>> gifs = userCache.getUserGifs(user, "query");

        assertFalse(gifs.isPresent());
    }

    @Test
    void whenContainsExistingGif_thenReturnTrue() {
        String user = "pasha";
        String query = "star";
        List<String> paths = new ArrayList<>();
        paths.add("path/to/star1");
        paths.add("path/to/star2");

        UserCache userCache = new UserCache();
        userCache.put(user, query, paths.get(0));
        userCache.put(user, query, paths.get(1));

        assertTrue(userCache.containsGif(user, query, paths.get(0)));
        assertTrue(userCache.containsGif(user, query, paths.get(1)));
    }

    @Test
    void whenContainsNonExistingGif_thenReturnFalse() {
        String user = "pasha";
        String query = "star";
        List<String> paths = new ArrayList<>();
        paths.add("path/to/star1");
        paths.add("path/to/star2");

        UserCache userCache = new UserCache();
        userCache.put(user, query, paths.get(0));
        userCache.put(user, query, paths.get(1));

        assertFalse(userCache.containsGif("user", query, paths.get(0)));
        assertFalse(userCache.containsGif(user, "query", paths.get(1)));
        assertFalse(userCache.containsGif(user, query, "wrong/path/to/gif"));
    }

    @Test
    void whenDeleteUseCache_thenReturnFalseForContains() {
        String user = "pasha";
        String query = "star";
        List<String> paths = new ArrayList<>();
        paths.add("path/to/star1");
        paths.add("path/to/star2");

        UserCache userCache = new UserCache();
        userCache.put(user, query, paths.get(0));
        userCache.put(user, query, paths.get(1));

        userCache.deleteUserCache(user);

        assertFalse(userCache.containsGif(user, query, paths.get(0)));
        assertFalse(userCache.containsGif(user, query, paths.get(1)));
    }

    @Test
    void whenDeleteUserQueryGifs_thenReturnFalseForContains() {
        String user = "pasha";
        String query = "star";
        String query2 = "sky";
        List<String> paths = new ArrayList<>();
        paths.add("path/to/star1");
        paths.add("path/to/star2");
        List<String> paths2 = new ArrayList<>();
        paths2.add("path/to/sky1");
        paths2.add("path/to/sky2");

        UserCache userCache = new UserCache();
        userCache.put(user, query, paths.get(0));
        userCache.put(user, query, paths.get(1));
        userCache.put(user, query2, paths2.get(0));
        userCache.put(user, query2, paths2.get(1));

        userCache.deleteUserQueryGifs(user, query);

        assertFalse(userCache.containsGif(user, query, paths.get(0)));
        assertFalse(userCache.containsGif(user, query, paths.get(1)));

        assertTrue(userCache.containsGif(user, query2, paths2.get(0)));
        assertTrue(userCache.containsGif(user, query2, paths2.get(1)));
    }

}
