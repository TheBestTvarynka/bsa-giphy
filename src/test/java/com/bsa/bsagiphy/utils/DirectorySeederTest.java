package com.bsa.bsagiphy.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

@WebMvcTest(DirectorySeeder.class)
public class DirectorySeederTest {

    @Value("${root_dir}")
    String rootDir;

    @Value("${cache_dir}")
    private String cacheDir;

    @Value("${users_dir}")
    private String usersDir;

    @Autowired
    DirectorySeeder directorySeeder;

    @Test
    void whenSeed_thenCreateDirectories() {
        directorySeeder.seed(null);

        assertTrue(Files.exists(Paths.get(cacheDir)));
        assertTrue(Files.exists(Paths.get(usersDir)));

        try {
            Files.walk(Paths.get(rootDir))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            fail(e);
        }
    }

}
