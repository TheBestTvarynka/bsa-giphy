package com.bsa.bsagiphy.utils;

import static org.junit.jupiter.api.Assertions.*;

import com.bsa.bsagiphy.utils.exceptions.BadUserIdException;
import org.junit.jupiter.api.Test;

public class ValidatorTest {

    @Test
    void whenValidateUsernameWithSpecialCharacters_thenThrowException() {
        assertThrows(BadUserIdException.class, () -> Validator.validateUserId("user?"));
        assertThrows(BadUserIdException.class, () -> Validator.validateUserId("?user"));
        assertThrows(BadUserIdException.class, () -> Validator.validateUserId("use*r"));
        assertThrows(BadUserIdException.class, () -> Validator.validateUserId("us%%er?"));
        assertThrows(BadUserIdException.class, () -> Validator.validateUserId("us,er"));
    }

    @Test
    void whenValidateValidUsername_thenDoNothing() {
        assertDoesNotThrow(() -> Validator.validateUserId("us.et"));
        assertDoesNotThrow(() -> Validator.validateUserId("user.name.test"));
        assertDoesNotThrow(() -> Validator.validateUserId("I_do_not_know"));
        assertDoesNotThrow(() -> Validator.validateUserId("micko-macko"));
    }

}
