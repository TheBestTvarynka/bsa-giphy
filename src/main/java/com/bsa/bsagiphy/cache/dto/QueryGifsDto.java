package com.bsa.bsagiphy.cache.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class QueryGifsDto {
    public String query;
    public List<String> gifs;
}
