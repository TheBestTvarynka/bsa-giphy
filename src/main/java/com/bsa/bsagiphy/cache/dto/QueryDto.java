package com.bsa.bsagiphy.cache.dto;

import lombok.Data;

@Data
public class QueryDto {
    public String query;
}
