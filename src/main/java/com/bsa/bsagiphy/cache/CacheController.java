package com.bsa.bsagiphy.cache;

import com.bsa.bsagiphy.cache.dto.QueryDto;
import com.bsa.bsagiphy.cache.dto.QueryGifsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cache")
public class CacheController {

    @Autowired
    private CacheService cacheService;

    @GetMapping
    public List<QueryGifsDto> findGifInCache(@RequestParam(required = false) String query) {
        return cacheService.findGifsByQuery(query);
    }

    @PostMapping("/generate")
    public QueryGifsDto generateNewGif(@RequestBody QueryDto query) {
        return cacheService.generateNewGif(query.query);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void clearCache() {
        cacheService.clearChache();
    }

}
