package com.bsa.bsagiphy.cache;

import com.bsa.bsagiphy.cache.dto.QueryGifsDto;
import com.bsa.bsagiphy.giphy.GiphyService;
import com.bsa.bsagiphy.giphy.dto.SimpleImageDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CacheService {
    private  static Logger logger = LogManager.getLogger(CacheService.class);

    @Autowired
    private GiphyService giphyService;
    @Value("${cache_dir}")
    private String cacheDir;

    public List<QueryGifsDto> findGifsByQuery(String query) {
        if (query != null) {
            try {
                return List.of(new QueryGifsDto(query, findGifsByKeyword(query)));
            } catch (NullPointerException e) {
                logger.error("cache folder for specific query does not exist. query:" + query);
                return new ArrayList<>();
            }
        }
        try {
            return Stream.of(new File(cacheDir).listFiles())
                    .map(file -> new QueryGifsDto(file.getName(), findGifsByKeyword(file.getName())))
                    .collect(Collectors.toList());
        } catch (NullPointerException e) {
            logger.error("cache folder does not exist: " + cacheDir);
            return new ArrayList<>();
        }
    }

    public List<String> findGifsByKeyword(String query) {
        return Stream.of(new File(cacheDir + query).listFiles())
                .map(file -> cacheDir + query + "/" + file.getName())
                .collect(Collectors.toList());
    }

    public QueryGifsDto generateNewGif(String query) {
        // searching gif on the giphy.com
        SimpleImageDto image = giphyService.getRandomGif(query);
        // downloading gif data
        byte[] imageData = giphyService.downloadGif(image.getUrl());

        // create directory for gif if it does not exist
        Path queryCacheDir = Paths.get(cacheDir + query);
        try {
            if (!Files.exists(queryCacheDir)) {
                Files.createDirectories(queryCacheDir);
            }
        } catch (IOException e) {
            logger.error("unable create query folder for gifs. query: " + query);
        }
        // save gif data in file
        try (FileOutputStream fos = new FileOutputStream(queryCacheDir.toString() + "/" + image.getId() + ".gif")) {
            fos.write(imageData);
        } catch (IOException e) {
            logger.error("unable to write gif in file: " + e.getMessage());
        }
        return new QueryGifsDto(query, findGifsByKeyword(query));
    }

    public void clearChache() {
        try {
            Files.walk(Paths.get(cacheDir))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            logger.error("unable to delete cache folder: " + cacheDir);
        }
    }

    public List<String> findAllGifs() {
        try {
            return Stream.of(new File(cacheDir).listFiles())
                    .flatMap(queryDir -> Stream.of(queryDir.listFiles())
                            .map(file -> cacheDir + queryDir.getName() + "/" + file.getName()))
                    .collect(Collectors.toList());
        } catch (NullPointerException e) {
            logger.error("unable to read the files: " + cacheDir);
            return new ArrayList<>();
        }
    }

}
