package com.bsa.bsagiphy.giphy;

import com.bsa.bsagiphy.giphy.dto.SimpleImageDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GiphyService {
    private static Logger logger = LogManager.getLogger(GiphyService.class);

    @Value("${api_key}")
    private String apiKey;
    private String server = "https://api.giphy.com/v1/gifs/";
    private RestTemplate rest;

    public GiphyService() {
        this.rest = new RestTemplate();
    }

    public SimpleImageDto getRandomGif(String query) {
        HttpHeaders headers;
        headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        HttpEntity<String> requestEntity = new HttpEntity<>("", headers);
        ResponseEntity<String> responseEntity = rest.exchange(
                server + "random?"
                + "api_key=" + apiKey
                + "&tag=" + query
                + "&rating=g",
                HttpMethod.GET,
                requestEntity,
                String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleImageDto image = null;
        try {
            JsonNode json = objectMapper.readTree(responseEntity.getBody());
            JsonNode data = json.get("data");
            String imageId = data.get("id").asText();
            String imageUrl = data.get("images").get("downsized_large").get("url").asText();
            image = new SimpleImageDto(imageId, imageUrl);
        } catch (JsonProcessingException e) {
            logger.error("error with json: " + e.getMessage());
        }
        return image;
    }

    public byte[] downloadGif(String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.IMAGE_GIF_VALUE);
        headers.add("Accept", "*/*");
        HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
        ResponseEntity<byte[]> response = rest.exchange(
                url,
                HttpMethod.GET,
                requestEntity,
                byte[].class);
        return response.getBody();
    }

}
