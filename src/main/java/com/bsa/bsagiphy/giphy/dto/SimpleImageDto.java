package com.bsa.bsagiphy.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class SimpleImageDto {
    String id;
    String url;
}
