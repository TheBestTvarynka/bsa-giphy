package com.bsa.bsagiphy.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(0)
public class RequestFilter implements Filter {

    @Value("${X_BSA_GIPHY}")
    private String X_BSA_GIPHY;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest)servletRequest;
        String header = httpRequest.getHeader("X-BSA-GIPHY");
        HttpServletResponse  response= (HttpServletResponse) servletResponse;
        if (header == null) {
            response.sendError(HttpStatus.FORBIDDEN.value(), "missing X-BSA-GIPHY header in request");
            return;
        }
        if (!header.equals(X_BSA_GIPHY)) {
            response.sendError(HttpStatus.FORBIDDEN.value(), "wrong X-BSA-GIPHY header");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
