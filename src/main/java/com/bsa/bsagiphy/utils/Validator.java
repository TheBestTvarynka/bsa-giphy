package com.bsa.bsagiphy.utils;

import com.bsa.bsagiphy.utils.exceptions.BadUserIdException;

public class Validator {
    public static void validateUserId(String userId) {
        if (!userId.matches("^(?![\\s.]+$)[a-zA-Z\\s._-]*$")) {
            throw new BadUserIdException();
        }
    }
}
