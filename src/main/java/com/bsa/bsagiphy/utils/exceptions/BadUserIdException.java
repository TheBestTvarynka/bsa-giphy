package com.bsa.bsagiphy.utils.exceptions;

public class BadUserIdException extends RuntimeException {

    public BadUserIdException() {
        super("Error: Bad userId!");
    }

    public BadUserIdException(String message) {
        super(message);
    }

}
