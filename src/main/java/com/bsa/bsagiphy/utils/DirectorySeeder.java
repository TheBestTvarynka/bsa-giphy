package com.bsa.bsagiphy.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class DirectorySeeder {
    private static Logger logger = LogManager.getLogger(DirectorySeeder.class);

    @Value("${cache_dir}")
    private String cacheDir;

    @Value("${users_dir}")
    private String usersDir;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        try {
            Path cacheDirPath = Paths.get(cacheDir);
            if (!Files.exists(cacheDirPath)) {
                Files.createDirectories(cacheDirPath);
            }
            Path usersDirPath = Paths.get(usersDir);
            if (!Files.exists(usersDirPath)) {
                Files.createDirectories(usersDirPath);
            }
        } catch (IOException e) {
            logger.error("unable to create base directories: " + e.getMessage());
        }
    }
}
