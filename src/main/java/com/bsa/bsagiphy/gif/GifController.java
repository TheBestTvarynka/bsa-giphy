package com.bsa.bsagiphy.gif;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GifController {

    @Autowired
    private GifService gifService;

    @GetMapping("/gifs")
    public List<String> getAllGifs() {
        return gifService.getAllCachedGifs();
    }

}
