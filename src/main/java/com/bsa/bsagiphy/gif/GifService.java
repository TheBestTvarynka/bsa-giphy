package com.bsa.bsagiphy.gif;

import com.bsa.bsagiphy.cache.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GifService {

    @Autowired
    private CacheService cacheService;

    public List<String> getAllCachedGifs() {
        return cacheService.findAllGifs();
    }

}
