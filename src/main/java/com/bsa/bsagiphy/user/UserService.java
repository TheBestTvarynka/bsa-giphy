package com.bsa.bsagiphy.user;

import com.bsa.bsagiphy.cache.CacheService;
import com.bsa.bsagiphy.cache.dto.QueryGifsDto;
import com.bsa.bsagiphy.user.dto.HistoryActionDto;
import com.bsa.bsagiphy.utils.Validator;
import com.bsa.bsagiphy.utils.exceptions.BadUserIdException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserService {
    private static Logger logger = LogManager.getLogger(UserService.class);

    @Value("${users_dir}")
    private String usersDir;
    @Autowired
    private UserCache userCache;
    @Autowired
    private CacheService cacheService;

    public List<QueryGifsDto> findAllUserGifs(String userId) {
        try {
            Validator.validateUserId(userId);
            return Stream.of(new File(usersDir + userId).listFiles())
                    .filter(File::isDirectory)
                    .map(file -> new QueryGifsDto(file.getName(), findUserGifsByKeyword(userId, file.getName())))
                    .collect(Collectors.toList());
        } catch (NullPointerException e) {
            logger.error("user's folder does not exist: " + usersDir + userId);
        } catch (BadUserIdException e) {
            logger.error(e.getMessage() + " : " + userId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return new ArrayList<>();
    }

    public List<String> findUserGifsByKeyword(String userId, String query) {
        try {
            Validator.validateUserId(userId);
            return Stream.of(new File(usersDir + userId + "/" + query).listFiles())
                    .map(file -> usersDir + query + "/" + file.getName())
                    .collect(Collectors.toList());
        } catch (NullPointerException e) {
            logger.error("folder with user's query gifs does not exist: " + usersDir + userId + "/" + query);
        } catch (BadUserIdException e) {
            logger.error(e.getMessage() + " : " + userId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return new ArrayList<>();
    }

    public List<HistoryActionDto> findAllUserActions(String userId) {
        try {
            Validator.validateUserId(userId);
            return Files.lines(Paths.get(usersDir + userId + "/history.csv"))
                    .filter(line -> !line.equals(""))
                    .map(HistoryActionDto::fromString)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            logger.error("file with user's history does not exist: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    public void clearUserHistory(String userId) {
        try {
            Validator.validateUserId(userId);
            FileOutputStream fout = new FileOutputStream(usersDir + userId + "/history.csv");
            fout.write("".getBytes());
            fout.close();
        } catch (IOException e) {
            logger.error("unable to clear cache of the user: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    public String searchUserGif(String userId, String query, Boolean force) {
        try {
            Validator.validateUserId(userId);
        } catch (BadUserIdException e) {
            logger.error(e.getMessage() + " : " + userId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        if (force != null && force) {
            return searchUserGifOnDisk(userId, query);
        }
        // try to find gif in cache
        Optional<List<String>> chachedUserGifs = userCache.getUserGifs(userId, query);
        if (chachedUserGifs.isEmpty()) {
            return searchUserGifOnDisk(userId, query);
        }
        logger.info("get gif from user's cache");
        List<String> gifs = chachedUserGifs.get();
        Random rand = new Random();
        return  gifs.get(rand.nextInt(gifs.size()));
    }

    public String searchUserGifOnDisk(String userId, String query) {
        logger.info("search gif on the disk");
        List<String> userQueryGifs = findUserGifsByKeyword(userId, query);
        if (userQueryGifs.size() < 1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gif not found");
        }
        Random rand = new Random();
        String gifUrl = userQueryGifs.get(rand.nextInt(userQueryGifs.size()));
        // save gif in cache
        userCache.put(userId, query, gifUrl);
        return  gifUrl;
    }

    public String generateGif(String userId, String query, Boolean force) {
        try {
            Validator.validateUserId(userId);
        } catch (BadUserIdException e) {
            logger.error(e.getMessage() + " : " + userId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        QueryGifsDto gifs;
        List<QueryGifsDto> queryGifs = cacheService.findGifsByQuery(query);
        if ((force != null  && force) || queryGifs.size() < 1) {
            gifs =  generateGifFromGiphy(query);
        } else {
            logger.info("get gif from cache");
            gifs = queryGifs.get(0);
        }

        Random rand = new Random();
        List<String> cachedGifs = gifs.getGifs();
        String gifUrl = cachedGifs.get(rand.nextInt(cachedGifs.size()));

        // create new directory for gif if needed
        Path cachedGifPath = Paths.get(gifUrl);
        Path queryDir = Paths.get(usersDir + userId + "/" + query);
        try {
            if (!Files.exists(queryDir)) {
                Files.createDirectories(queryDir);
            }
        } catch (IOException e) {
            logger.error("unable to create folder for user's gifs: " + e.getMessage());
        }
        // copy gif from cache directory to user's directory
        Path resultGifUrl = Paths.get(queryDir.toString() + "/" + cachedGifPath.getName(cachedGifPath.getNameCount() - 1));
        try {
            Files.copy(cachedGifPath, resultGifUrl, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            logger.error("unable to copy cached gif file to user's folder" + e.getMessage());
        }
        UserLogger.info(usersDir + userId + "/history.csv",
                LocalDate.now().toString() + "," + query + "," + resultGifUrl + "\n");
        return resultGifUrl.toString();
    }

    public QueryGifsDto generateGifFromGiphy(String query) {
        logger.info("Download gif from giphy");
        return cacheService.generateNewGif(query);
    }

    public void clearUserCache(String userId, String query) {
        try {
            Validator.validateUserId(userId);
        } catch (BadUserIdException e) {
            logger.error(e.getMessage() + " : " + userId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        if (query == null) {
            userCache.deleteUserCache(userId);
        }
        userCache.deleteUserQueryGifs(userId, query);
    }

    public void deleteUserFiles(String userId) {
        try {
            Validator.validateUserId(userId);
        } catch (BadUserIdException e) {
            logger.error(e.getMessage() + " : " + userId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        clearUserCache(userId, null);
        try {
            Files.walk(Paths.get(usersDir + userId))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            logger.error("unable to delete user's files: " + e.getMessage());
        }
    }

}
