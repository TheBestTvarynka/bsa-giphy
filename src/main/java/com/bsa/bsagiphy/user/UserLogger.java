package com.bsa.bsagiphy.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;

public class UserLogger {
    private static Logger logger = LogManager.getLogger(UserService.class);

    public static void info(String filename, String message) {
        try {
            FileOutputStream fout = new FileOutputStream(filename, true);
            fout.write(message.getBytes());
            fout.close();
        } catch (IOException e) {
            logger.error(e);
        }
    }

}
