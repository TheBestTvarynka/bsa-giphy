package com.bsa.bsagiphy.user;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserCache {

    private Map<String, Map<String, List<String>>> cache;

    public UserCache() {
        cache = new HashMap<>();
    }

    public void put(String user, String query, String gifUrl) {
        if (!cache.containsKey(user)) {
            cache.put(user, new HashMap<>());
        }
        Map<String, List<String>> userGifs = cache.get(user);
        if (!userGifs.containsKey(query)) {
            userGifs.put(query, new ArrayList<>());
        }
        userGifs.get(query).add(gifUrl);
    }

    public Optional<List<String>> getUserGifs(String user, String query) {
        if (cache.containsKey(user)) {
            List<String> gifs = cache.get(user).get(query);
            if (gifs == null) {
                return Optional.empty();
            }
            return Optional.of(gifs);
        }
        return Optional.empty();
    }

    public boolean containsGif(String user, String query, String gifUrl) {
        if (cache.containsKey(user)) {
            Map<String, List<String>> userGifs = cache.get(user);
            if (userGifs.containsKey(query)) {
                return userGifs.get(query).contains(gifUrl);
            }
            return false;
        }
        return false;
    }

    public void deleteUserCache(String user) {
        cache.remove(user);
    }

    public void deleteUserQueryGifs(String user, String query) {
        if (cache.containsKey(user)) {
            Map<String, List<String>> userGifs = cache.get(user);
            userGifs.remove(query);
        }
    }

}
