package com.bsa.bsagiphy.user;

import com.bsa.bsagiphy.cache.dto.QueryGifsDto;
import com.bsa.bsagiphy.user.dto.GenerateGifParametersDto;
import com.bsa.bsagiphy.user.dto.HistoryActionDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    private static Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping("/{id}/all")
    public List<QueryGifsDto> getAllUserGifs(@PathVariable String id) {
        logger.info("extracting all user's gifs. user: " + id);
        return userService.findAllUserGifs(id);
    }

    @GetMapping("/{id}/history")
    public List<HistoryActionDto> getAllHistory(@PathVariable String id) {
        logger.info("extracting history for user: " + id);
        return userService.findAllUserActions(id);
    }

    @GetMapping("/{id}/search")
    public String searchGif(@PathVariable String id,
                            @RequestParam String query,
                            @RequestParam(required = false) Boolean force) {
        logger.info("searching gif query(" + query + ") for user: " + id);
        return userService.searchUserGif(id, query, force);
    }

    @PostMapping("/{id}/generate")
    public String generateUserGif(@PathVariable String id,
                                  @RequestBody GenerateGifParametersDto param) {
        logger.info("generating new gif for user(" + id + ") with parameters(" + param + ")");
        return userService.generateGif(id, param.getQuery(), param.getForce());
    }

    @DeleteMapping("/{id}/history/clean")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void clearHistory(@PathVariable String id) {
        logger.info("clearing history for user: " + id);
        userService.clearUserHistory(id);
    }

    @DeleteMapping("/{id}/reset")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void clearUserCache(@PathVariable String id,
                               @RequestParam(required = false) String query) {
        logger.info("clearing user cache for user: " + id);
        userService.clearUserCache(id, query);
    }

    @DeleteMapping("/{id}/clean")
    public void deleteUserFiles(@PathVariable String id) {
        userService.deleteUserFiles(id);
    }

}
