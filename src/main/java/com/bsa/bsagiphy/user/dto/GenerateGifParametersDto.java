package com.bsa.bsagiphy.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenerateGifParametersDto {
    public String query;
    public Boolean force;
}
