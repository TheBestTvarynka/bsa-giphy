package com.bsa.bsagiphy.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;

@Data
@AllArgsConstructor
public class HistoryActionDto {
    public String date;
    public String query;
    public String gif;

    public static HistoryActionDto fromString(String data) {
        String[] parts = data.split(",");
        return new HistoryActionDto(parts[0], parts[1], parts[2]);
    }

}
