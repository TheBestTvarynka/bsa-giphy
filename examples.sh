
# we will get empty cache
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/cache"

# generate some gifs
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"laptop"}' -H "Content-Type: application/json" -X POST http://localhost:8080/cache/generate
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"laptop"}' -H "Content-Type: application/json" -X POST http://localhost:8080/cache/generate
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"laptop"}' -H "Content-Type: application/json" -X POST http://localhost:8080/cache/generate
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"laptop"}' -H "Content-Type: application/json" -X POST http://localhost:8080/cache/generate

# get all generated gifs
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/cache"

curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"spring"}' -H "Content-Type: application/json" -X POST http://localhost:8080/cache/generate
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"star"}' -H "Content-Type: application/json" -X POST http://localhost:8080/cache/generate

# selecting some gifs
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/cache"
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/cache?query=star" 

# get all cached gifs
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/gifs"

# we will get nothing because user's files does not exist
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET http://localhost:8080/user/pasha/all
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET http://localhost:8080/user/pasha/history

# generate some gifs (see program logs for more details)
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"star"}' -H "Content-Type: application/json" -X POST http://localhost:8080/user/pasha/generate
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"laptop"}' -H "Content-Type: application/json" -X POST http://localhost:8080/user/pasha/generate
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"book"}' -H "Content-Type: application/json" -X POST http://localhost:8080/user/pasha/generate
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"book"}' -H "Content-Type: application/json" -X POST http://localhost:8080/user/pasha/generate
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -d '{"query":"book","force":"true"}' -H "Content-Type: application/json" -X POST http://localhost:8080/user/pasha/generate

# selecting all gifs
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET http://localhost:8080/user/pasha/all

# getting history of the user with id = pasha
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET http://localhost:8080/user/pasha/history

# searching gifs (see program log for more details)
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/user/pasha/search?query=book"
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/user/pasha/search?query=star"
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/user/pasha/search?query=star"
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/user/pasha/search?query=book"
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/user/pasha/search?query=book&force=true"
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/user/pasha/search?query=book"

# delete user's cache and select gif
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X DELETE "http://localhost:8080/user/pasha/reset"
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X GET "http://localhost:8080/user/pasha/search?query=book"

# delete all user's files and cache
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X DELETE "http://localhost:8080/user/pasha/clean"

# clear the cache
curl -H "X-BSA-GIPHY: bsa.giphy.amazing-world" -X DELETE "http://localhost:8080/cache"

# some wrong requests
# wrong header value
curl -H "X-BSA-GIPHY: bsa.giphy.amazingworld" -X GET http://localhost:8080/user/pasha/all
# missing header
curl -X GET http://localhost:8080/user/pasha/all
# try to generate gif for user with bad id
curl -X GET http://localhost:8080/user/pa,sha/all

